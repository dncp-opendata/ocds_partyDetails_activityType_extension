# Party Details - Activity Type

This is a Party Details extension. It adds support for the detailed classification of one or more of the organizations included in an OCDS contracting process release.

The `activityTypes` array should be included in a release at `parties/details/activityTypes`.

## Activity Types

The `activityTypes` property can be used to indicate the kind of commercial activity that a commercial organization does.

The extension uses a fixed codelist with the following values:

* goods
* services

If the organization does both types of activity, then both code should be used in the activityTypes array

## Example

The following example shows the location where the activityTypes extension can be used.

```json
{
"releases":[
    {
       "parties":[
          {
            "id": "GB-COH-1234567844",
            "name": "AnyCorp Cycle Provision",
            "details":{
               "activityTypes": ["goods", "services"]
            }
          }
       ]
    }
]
}
```

## Issues

Report issues for this extension in the [ocds-extensions repository](https://github.com/open-contracting/ocds-extensions/issues), putting the extension's name in the issue's title.
